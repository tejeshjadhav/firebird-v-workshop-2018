/*
 * 2_Bar_Graph_WD1.c
 *
 * Created: 1/21/2018 3:12:04 PM
 * Author : Tejesh Jadhav
 */ 
#define F_CPU 14745600L

#include <avr/io.h>
#include <util/delay.h>


int main(void)
{
    DDRJ = 0xFF;				//set the port as output
	PORTJ = 0x01;				//start with the 1st led
    while (1)					//the infinite loop
    {
		_delay_ms(1000);		//wait for 1 sec
		PORTJ = PORTJ << 1;		//move to next pin
		if (PINJ == 0x00)		//if the cycle is complete then
		{
			PORTJ = 0x01;		//go to the 1st pin
		}

		/*
		PORTJ = 0x00;
		_delay_ms(1000);
		PORTJ = 0x0F;			//turn on the lower nibble
		_delay_ms(1000);
		PORTJ = 0xF0;			//turn on the upper nibble
		_delay_ms(1000);		
		PORTJ = 0xFF;			//turn on the whole byte
		_delay_ms(1000);
		*/
    }
}

