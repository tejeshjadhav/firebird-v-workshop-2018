/*
 * ADC_wd2.cpp
 *
 * Created: 21-01-2018 18:28:48
 * Author : tanma
 */ 
#define F_CPU 1475600L
#include "lcd.h"
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

volatile int sen_num=0;
volatile int lsensor=0,csensor=0,rsensor=0;
void linesensor_setup()
{
	DDRC = 0x00;
	ADMUX = (1<<REFS0) | (1<<MUX0);
	ADCSRA = (1<<ADEN) | (1<<ADATE) |(1<<ADSC) | (1<<ADIE) | (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0);
}
void lcd_setup()
{
	DDRC = 0xF7;
	lcd_set_4bit();
	lcd_init();
}

int main(void)
{
	linesensor_setup();
    /* Replace with your application code */
    while (1) 
    {
		lcd_print(1,1,lsensor,3);
		lcd_string(" ");
		lcd_print(1,5,lsensor,3);
		lcd_string(" ");
		lcd_print(1,9,lsensor,3);
		lcd_string(" ");
    }
}

ISR(ADC_vect)
{
	if(sen_num==0)
	{
		lsensor=ADC;
		ADMUX &= ~(1<<MUX0);
		ADMUX |= (1<<MUX1);
		sen_num=1;
	}
	else if(sen_num==1)
	{
		csensor=ADC;
		ADMUX |= (1<<MUX0);
		sen_num=2;
	}
	else 
	{
		rsensor=ADC;
		ADMUX &= ~(1<<MUX1);
		sen_num=0;
	}
}
