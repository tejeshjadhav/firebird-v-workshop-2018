/*
 * encoder_wd2.cpp
 *
 * Created: 21-01-2018 16:02:29
 * Author : tanma
 */ 

#define F_CPU 14745600L
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "lcd.h"

volatile int lmotor=0,rmotor=0;

void run_motor()
{
	DDRA = 0xFF; //direction port as output
	DDRL = 0xFF; //pwm port as output
	
	
	
	TCCR5A = (1<<COM5A1)|(1<<COM5B1)|(1<<WGM50);//set compare output modes for motors
	OCR5A = 255;
	OCR5B = 255;//set motor speeds
	TCCR5B = (1<<WGM52) | (1<<CS50) | (1<<CS52);//set pwm mode
	
}
void encoder_setup()
{
	DDRE = 0x00;
	EICRB = (1<<ISC41) | (1<<ISC51);
	EIMSK = (1<<INT4) | (1<<INT5);
	sei();
}
void lcd_setup()
{
	DDRC = 0xF7;
	lcd_set_4bit();
	lcd_init();
}
void left_turn()
{	
	PORTA = 0x00;
	_delay_ms(500);
	PORTA =  (1<<PA2); //go left
	lmotor=rmotor=0;
	while(rmotor<=35);
	rmotor=0;
}
void forward()
{
	PORTA = (1<<PA1) | (1<<PA2); //go fwd
}

int main(void)
{
	/* Replace with your application code */
	lcd_setup();
	encoder_setup();
	while (1)
	{
		run_motor();
		lcd_cursor(1,1);
		lcd_string("left motor:");
		lcd_print(1,12,lmotor,4);
		lcd_cursor(2,1);
		lcd_string("right motor:");
		lcd_print(2,12,rmotor,4);
		if(lmotor>300 || rmotor>300)
		{
			left_turn();
		}
		else forward();
		
	}
}
ISR(INT4_vect)
{
	lmotor++;
}
ISR(INT5_vect)
{
	rmotor++;
}


