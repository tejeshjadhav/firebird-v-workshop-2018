/*
 * IR_sensor_wd3.cpp
 *
 * Created: 22-01-2018 19:34:24
 * Author : tanma
 */ 
#define F_CPU 14745600L

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "lcd.h"

volatile int sennum=0;
volatile int ir_sensor[5];
void lcd_setup()
{
	DDRC = 0xF7;
	lcd_set_4bit();
	lcd_init();
}
void irsensor_setup()
{
	DDRF = 0x00;
	DDRK = 0x00;
	ADMUX = (1<<REFS0) | (1<<MUX2);
	ADCSRA = (1<<ADEN) | (1<<ADATE) |(1<<ADSC) | (1<<ADIE) | (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0);
	sei();
}
int main(void)
{
	lcd_setup();
	irsensor_setup();
    /* Replace with your application code */
    while (1) 
    {
		for(int i=0;i<=4;i++)
		{
			lcd_print(1,(i*2)+1+i,ir_sensor[i],2);
		}
		
    }
}
ISR(ADC_vect)
{
	if(sennum==0)
	{
		ir_sensor[sennum]=ADC;
		ADCSRA |= (1<<MUX0);
		sennum=1;
	}
	else if(sennum==1)
	{
		ir_sensor[sennum]=ADC;
		ADCSRA &= ~(1<<MUX0);
		ADCSRA |= (1<<MUX1);
		sennum=2;
	}
	else if(sennum==2)
	{
		ir_sensor[sennum]=ADC;
		ADCSRA |= (1<<MUX0) ;
		sennum=3;
	}
	else if(sennum==3)
	{
		ir_sensor[sennum]=ADC;
		ADCSRA &= ~((1<<MUX0) | (1<<MUX1) | (1<<MUX2));
		ADCSRB |= (1<<MUX5);
		sennum=4;
	}
	else
	{
		ir_sensor[sennum]=ADC;
		ADCSRB &= ~(1<<MUX5);
		ADCSRA |= (1<<MUX2);
		sennum=0;
	}

	
}
