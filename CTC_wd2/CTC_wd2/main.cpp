/*
 * CTC_wd2.cpp
 *
 * Created: 21-01-2018 13:58:10
 * Author : tanma
 */ 
#define F_CPU 14745600L
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

void CTC_setup()
{
	TCCR1A = (1<<COM1A1);  //pg 155 
	TIMSK1 = (1<<OCIE1A);  //enable output compare interrupt
	OCR1A = 14400; //timer count
	TCCR1B = (1<<WGM12)|(1<<CS10) | (1<<CS12);//set prescalar and set ctc mode
	DDRC = 0xFF; //set port as output	
	sei();//enable global interrupts
}

int main(void)
{
	CTC_setup();  
	DDRJ = 0xFF;
    /* Replace with your application code */
    while (1) 
    {
		PORTJ ^=(1<<0);
		_delay_ms(1000);
    }
}

ISR(TIMER1_COMPA_vect)//isr function 
{
	PORTC ^= (1<<3);//toggle pin
}

